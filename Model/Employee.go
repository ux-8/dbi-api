package Model

import "time"

type Employee struct {
	ID        int       `form : id json : id`
	Name      string    `form : name json : name`
	Email     string    `form : email json : email`
	BirthDay  time.Time `form : birthday json : birthday`
	Education string    `form : education json : education`
	Part      string    `form : part json : part`
	Address   string    `form : address json : address`
	Country   string    `form : country json : country`
	Region    string    `form : region json : region`
	City      string    `form : city json : city`
	Username  string    `form : username json : username`
	Password  string    `form : password json : password`
}

type EmployeeResponse struct {
	Status  int        `form : "status" json:"status"`
	Message string     `form : "message" json:"message"`
	Data    []Employee `form : "data" json:"data"`
}
