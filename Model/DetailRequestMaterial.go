package Model

type DetailRequestMaterial struct {
	ID       int      `form : id json : id`
	Material Material `form : material json : material`
	Amount   int      `form : amount json : amount`
}

type DetailRequestMaterialResponse struct {
	Status  int                     `form : "status" json:"status"`
	Message string                  `form : "message" json:"message"`
	Data    []DetailRequestMaterial `form : "data" json:"data"`
}
