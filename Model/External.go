package Model

type External struct {
	ID           int           `form : id json : id`
	Name         string        `form : name json : name`
	Address      string        `form : address json : address`
	City         string        `form : city json : city`
	Region       string        `form : region json : region`
	Country      string        `form : country json : country`
	PostalCode   string        `form : postalcode json : postalcode`
	Type         string        `form : type json : type`
	WorkingHours []WorkingHour `form : workinghours json : workinghours`
}

type ExternalResponse struct {
	Status  int        `form : "status" json:"status"`
	Message string     `form : "message" json:"message"`
	Data    []External `form : "data" json:"data"`
}
