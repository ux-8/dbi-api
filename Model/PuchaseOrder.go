package Model

import "time"

type PurchaseOrder struct {
	ID              int              `form : id json : id`
	Date            time.Time        `form : date json : date`
	Vendor          External         `form : vendor json : vendor`
	PurchaseDetails []PurchaseDetail `form : purchasedetails json : purchasedetails`
}

type PurchaseOrderResponse struct {
	Status  int             `form : "status" json:"status"`
	Message string          `form : "message" json:"message"`
	Data    []PurchaseOrder `form : "data" json:"data"`
}
