package Model

import "time"

type Invoice struct {
	ID            int           `form : id json : id`
	Date          time.Time     `form : date json : date`
	PurchaseOrder PurchaseOrder `form : purchaseorder json : purchaseorder`
	SalesOrder    SalesOrder    `form : salesorder json : salesorder`
}

type InvoiceResponse struct {
	Status  int       `form : "status" json:"status"`
	Message string    `form : "message" json:"message"`
	Data    []Invoice `form : "data" json:"data"`
}
