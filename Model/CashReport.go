package Model

import "time"

type CashReport struct {
	ID           int       `form : id json : id`
	Date         time.Time `form : date json : date`
	Status       string    `form : status json : status`
	Descriptions string    `form : description json : description`
	Amount       int       `form : amount json : amount`
	Person       Employee  `form : person json : person`
}

type CashReportResponse struct {
	Status  int          `form : "status" json:"status"`
	Message string       `form : "message" json:"message"`
	Data    []CashReport `form : "data" json:"data"`
}
