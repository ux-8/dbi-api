package Model

import "time"

type WorkingHour struct {
	ID      int       `form : id json : id`
	Day     string    `form : day json : day`
	TimeIn  time.Time `form : timein json : timein`
	TimeOut time.Time `form : timeout json : timeout`
}

type WrokingHoursResponse struct {
	Status  int           `form : "status" json:"status"`
	Message string        `form : "message" json:"message"`
	Data    []WorkingHour `form : "data" json:"data"`
}
