package Model

type BOM struct {
	ID              int        `form : id json : id`
	ListMaterial    []Material `form : listmaterial json : listmaterial`
	ProduceMaterial Material   `form : producematerial json : producematerial`
}

type BOMResponse struct {
	Status  int    `form : "status" json:"status"`
	Message string `form : "message" json:"message"`
	Data    []BOM  `form : "data" json:"data"`
}
