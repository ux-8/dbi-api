package Model

import "time"

type ProduceGoods struct {
	ID       int       `form : id json : id`
	Date     time.Time `form : date json : date`
	Employee Employee  `form : employee json : employee`
	BOM      BOM       `form : bom json : bom`
	Quantity int       `form : quantity json : quantity`
}

type ProduceGoodsResponse struct {
	Status  int            `form : "status" json:"status"`
	Message string         `form : "message" json:"message"`
	Data    []ProduceGoods `form : "data" json:"data"`
}
