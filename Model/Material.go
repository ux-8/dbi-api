package Model

type Material struct {
	ID          int    `form : id json : id`
	Name        string `form : name json : name`
	Quantity    int    `form : quantity json : quantity`
	Price       int    `form : price json : price`
	Type        string `form : type json : type`
	Measurement string `form : measurement json : measurement`
}

type MaterialResponse struct {
	Status  int        `form : "status" json:"status"`
	Message string     `form : "message" json:"message"`
	Data    []Material `form : "data" json:"data"`
}
