package Model

type SalesDetail struct {
	ID         int        `form : id json : id`
	Material   Material   `form : material json : material`
	SalesOrder SalesOrder `form : salesorder json : salesorder`
	Quantity   int        `form : quantity json : quantity`
	Price      int        `form : price json : price`
}

type SalesDetailResponse struct {
	Status  int           `form : "status" json:"status"`
	Message string        `form : "message" json:"message"`
	Data    []SalesDetail `form : "data" json:"data"`
}
