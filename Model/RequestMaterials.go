package Model

import "time"

type RequestMaterials struct {
	ID                int                     `form : id json : id`
	Date              time.Time               `form : date json : date`
	IsAccepted        bool                    `form : isaccepted json : isaccepted`
	ListDetailRequest []DetailRequestMaterial `form : listdetailrequest json : listdetailrequest`
	Type              string                  `form : type json : type`
}

type RequestMaterialsResponse struct {
	Status  int                `form : "status" json:"status"`
	Message string             `form : "message" json:"message"`
	Data    []RequestMaterials `form : "data" json:"data"`
}
